package com.devcamp.c50.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.c50.customeraccountapi.models.Account;

@Service
public class AccountService {
    @Autowired
    CustomerService customerService;

    public ArrayList<Account> getAllAccount(){
        ArrayList<Account> allAccount = new ArrayList<Account>();

        allAccount.add(new Account(1, customerService.customer1, 100000));
        allAccount.add(new Account(2, customerService.customer2, 200000));
        allAccount.add(new Account(3, customerService.customer3, 300000));
        
        return allAccount;
    }

}
