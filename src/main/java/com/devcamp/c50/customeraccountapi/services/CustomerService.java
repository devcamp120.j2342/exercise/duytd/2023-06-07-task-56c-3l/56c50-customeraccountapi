package com.devcamp.c50.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.c50.customeraccountapi.models.Customer;

@Service
public class CustomerService {

    Customer customer1 = new Customer(1, "Nguyen Van A", 10);
    Customer customer2 = new Customer(2, "Nguyen Van B", 20);
    Customer customer3 = new Customer(3, "Nguyen Van C", 30);

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> allCustomers = new ArrayList<Customer>();

        allCustomers.add(customer1);
        allCustomers.add(customer2);
        allCustomers.add(customer3);

        return allCustomers;
    }
}
