package com.devcamp.c50.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c50.customeraccountapi.models.Account;
import com.devcamp.c50.customeraccountapi.services.AccountService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AccountController {
    @Autowired
    AccountService accountService;

    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount(){
        ArrayList<Account> allAccount = accountService.getAllAccount();

        return allAccount;
    }
}
