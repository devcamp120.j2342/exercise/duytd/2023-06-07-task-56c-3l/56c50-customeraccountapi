package com.devcamp.c50.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c50.customeraccountapi.models.Customer;
import com.devcamp.c50.customeraccountapi.services.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> allCustomers = customerService.getAllCustomers();

        return allCustomers;
    }
   
}
